#!/bin/bash

facture=facture_type.ods

if [ ! -e $facture ]
then
    echo "no such file named as: $facture"
    echo "did you removed it ?"
    exit
fi

# for arg in $@
# do
#     if [ `echo $arg | head -c 1` = "-" ]
#     then
#         echo $arg 
#     fi 
#     ((t++))
# done

if [ $# -ne 4 ]
then
    echo "Expected 4 arguments, got $#"
    echo "Usage: $0 <FactureNumber> <dailyAmount> <StartDate> <NumberOfDaysOfActivity>"
    exit
fi

if [ ! -e tmp ] && [ ! -d tmp ]
then
    mkdir tmp
fi

nb="$1"
# nb=`ls -l facture*pdf | wc -l`
echo $nb

cp $facture test.ods
cd tmp

unzip ../test.ods
echo "######################################################################################"

sed -i "s/NUMEROFLAG/$1/" content.xml
sed -i "s/PRIXFLAG/$2/" content.xml 
sed -i "s/MONTANTFLAG/$(($2*$4))/" content.xml
sed -i "s/TOTALFLAG/$(($2*$4))/" content.xml  
sed -i "s/DATEFLAG/$(date '+%d -%m -%Y')/" content.xml

fdate=`date -d "$3+$(($4-1)) days" "+%d -%m -%Y"`
echo $fdate

sed -i "s/DATE1FLAG/`date -d $3 "+%d -%m -%Y"`/" content.xml
sed -i "s/DATE2FLAG/$fdate/" content.xml  

echo "######################################################################################"
zip -r ../test.ods .
echo "######################################################################################"
cd ..

facture_name=facture$nb
while [ -f $facture_name.pdf ]
do
    facture_name=$facture_name-$RANDOM
done

mv test.ods $facture_name.ods

libreoffice --headless --convert-to pdf $facture_name.ods 
rm -rf tmp && rm $facture_name.ods
firefox $facture_name.pdf
