#!/usr/bin/env python3

"""

Ce programme est publié sous la licence Creative Commons CC0 1.0
consultable à cette adresse :
<https://creativecommons.org/publicdomain/zero/1.0/legalcode.fr>

"""

import getopt
from sys import argv
from sys import stderr
from datetime import date
from datetime import timedelta
import subprocess
import locale
import json
import os
from functools import reduce

version = "1.0"

def lire_arguments(arguments):
    optionsl = ['numéro=', 'montant-journalier=', 'dates-début=', 'aide',
                'nb-jours=', 'format=', 'intitulés=', 'fichier-configuration=',
                'visualiser', 'help', 'version']
    listeopt, args = getopt.getopt(arguments, 'n:m:d:j:f:i:cahv', optionsl)
    return listeopt

def afficher_version():
    print('{} {}'.format(argv[0], version))

def afficher_aide():
    print('\nUtilisation : {} OPTION…'.format(argv[0]))
    print('Génère une facture en utilisant LaTeX.\n')
    print('--help, --aide, -h, -a')
    print('              afficher cette aide et quitter')
    print('--version, -v afficher la version et quitter')
    print('--numéro=NUM, -n NUM')
    print('                spécifier le numéro de facture')
    print('--montant-journalier=MONTANT, -m MONTANT')
    print('                spécifier le montant journalier, par défaut 150 €')
    print('--dates-début=\'([AAAA-MM-JJ])|[MM-]JJ;...\', -d \'([AAAA-MM-JJ])|[MM-]JJ;...\'')
    print('                spécifier les dates de débuts des prestations,')
    print('                  si l’année ou le mois n’est pas spécifié,')
    print('                  l’année ou le mois courant sera utilisé')
    print('--nb-jours=\'NOMBRE;...\', -j \'NOMBRE;...\'')
    print('                spécifier les nombre de jours travaillé à partir des dates')
    print('                  de début')
    print('--format=<latex|odf>, -f <latex|odf>')
    print('                spécifier la manière de génération de la facture,')
    print('                  LaTeX par défaut')
    print('--intitulé=\'INTITULÉ;...\', -i \'INTITULÉ...\'')
    print('                les intitulés des prestations')
    print('-c')
    print('                compiler le ficher LaTeX après la génération')
    print('--fichier-configuration=CHEMIN')
    print('                spécifier le chemin du fichier de configuration,')
    print('                  (« ./configuration.json » par défaut')
    print('--visualiser    permet de visualiser le fichier créé avec le lecteur')
    print('                  PDF renseigné dans le fichier de configuration')
    print('                  (evince étant le lecteur par défaut)')

def traiter_date(date_utilisateur):
    éléments = date_utilisateur.split('-')
    la_date = date.today()

    try:
        if len(éléments) >= 1:
            la_date = la_date.replace(day=int(éléments[len(éléments) - 1]))
        if len(éléments) >= 2:
            la_date = la_date.replace(mounth=int(éléments[len(éléments) - 2]))
        if len(éléments) >= 3:
            la_date = la_date.replace(year=int(éléments[len(éléments) - 3]))
    except:
        print('Une erreur est survenue lors du traitement de la date.', file=stderr)
        afficher_aide()
        exit(1)

    return la_date.isoformat()


def traiter_arguments(listeoptions):
    dico = {'numéro': None, 'montant-journalier': 150, 'dates-début': None,
            'nb-jours': None, 'format': 'latex', 'intitulés': None,
            'compiler': False, 'fichier de configuration': 'configuration.json',
            'visualiser': False}

    for o, a in listeoptions:
        if o in ('-h', '--help', '-a', '--aide'):
            afficher_aide()
            exit(0)
        elif o in ('-v', '--version'):
            afficher_version()
            exit(0)
        elif o in ('--numéro', '-n'):
            dico['numéro'] = int(a)
        elif o in ('--montant-journalier', '-m'):
            dico['montant-journalier'] = int(a)
        elif o in ('--dates-début', '-d'):
            dico['dates-début'] = list(map(lambda x: traiter_date(x.strip()),
                                      a.split(';')))
        elif o in ('--nb-jours', '-j'):
            dico['nb-jours'] = list(map(lambda x: int(x), a.split(';')))
        elif o in ('--format', '-f'):
            dico['format'] = a
        elif o in ('--intitulés', '-i'):
            dico['intitulés'] = a.split(';')
        elif o == '-c':
            dico['compiler'] = True
        elif o == '--fichier-configuration':
            dico['fichier de configuration'] = a
        elif o == '--visualiser':
            dico['visualiser'] = True

    for clef, valeur in dico.items():
        if valeur is None:
            afficher_aide()
            exit(1)

    if dico['format'] not in ('latex', 'odf'):
        afficher_aide()
        exit(1)

    # Calcul des dates de fin
    dico['dates-fin'] = list(map(lambda x, y: (date.fromisoformat(x) + timedelta(days=y)).isoformat(),
                                 dico['dates-début'],
                                 list(map(lambda x: x - 1, dico['nb-jours']))))

    return dico

def date_lisible(la_date):
    return la_date.strftime('%A %d %B %Y')

def générer_lignes(configuration):
    ligne = '\ligne{{Formation « {} », forfait journalier: {} € (du {} au {})}}[1]{{{}}}\n'

    lignes = list(map(lambda w, x, y, z: ligne.format(w, configuration['montant-journalier'],
                                                      date_lisible(date.fromisoformat(x)),
                                                      date_lisible(date.fromisoformat(y)),
                                                      z * configuration['montant-journalier']),
                      configuration['intitulés'], configuration['dates-début'],
                      configuration['dates-fin'], configuration['nb-jours']))

    return lignes

def générer_facture_latex(configuration):
    modèle = ''

    with open('modèle.tex', 'r') as f:
        modèle += f.read()

    modèle = modèle.replace('NUMFACTURE', str(configuration['numéro']))
    modèle = modèle.replace('MONTANTJOURNALIER', str(configuration['montant-journalier']))
    modèle = modèle.replace('NOM', configuration['nom'])
    modèle = modèle.replace('NUMSIREN', configuration['SIREN'])
    modèle = modèle.replace('ADRESSEÉLECTRONIQUE', configuration['adresse électronique'])
    modèle = modèle.replace('TÉLÉPHONE', configuration['téléphone'])
    modèle = modèle.replace('LIGNES', reduce(lambda x, y: x + y, générer_lignes(configuration)))

    # Les adresses
    adresse = ''
    for élément in configuration['adresse']:
        adresse += élément + '\\\\\n'
    modèle = modèle.replace('ADRESSE', adresse)

    entreprise = ''
    for élément in configuration['adresse facturation']:
        entreprise += élément + '\\\\\n'
    modèle = modèle.replace('ENTREPRISE', entreprise)

    return modèle

def générer_facture_odf(arguments):
    subprocess.run(['./script.sh', str(arguments['numéro']), str(arguments['montant-journalier']),
                    arguments['dates-début'][0], str(arguments['nb-jours'][0])])

def lire_configuration(chemin):
    if not os.path.isfile(chemin):
        print('Veuillez créer le fichier de configuration « {} »'.format(chemin), file=stderr)
        exit(1)

    dico = {'nom': None, 'SIREN': None, 'adresse': None, 'téléphone': None,
            'adresse électronique': None, 'adresse facturation': None,
            'lecteur PDF': 'evince'}

    with open(chemin, 'r') as f:
        document = json.loads(f.read())
        clefs = ['nom', 'SIREN', 'adresse', 'téléphone', 'adresse électronique',
                 'adresse facturation', 'lecteur PDF']
        if not all(elem in document.keys() for elem in clefs):
            afficher_aide()
            exit(1)
        else:
            dico = document.copy()

    if None in dico.keys():
        afficher_aide()
        exit(1)

    return dico

def début(arguments):
    locale.setlocale(locale.LC_ALL, '')

    args = traiter_arguments(lire_arguments(arguments[1:]))
    config = lire_configuration(args['fichier de configuration'])
    config.update(args)

    nom_facture = 'facture' + str(config['numéro'])
    if config['format'] == 'latex':
        facture = générer_facture_latex(config)
        with open('{}.tex'.format(nom_facture), 'w') as f:
            f.write(facture)
        if config['compiler']:
            subprocess.run(['xelatex',  '\\nonstopmode\\input',
                            '{}.tex'.format(nom_facture)])
    else:
        générer_facture_odf(config)

    if config['visualiser']:
        subprocess.run([config['lecteur PDF'], '{}.pdf'.format(nom_facture)])


if __name__ == "__main__":
    début(argv)
